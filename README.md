# Matrix Bit Animation

This cool program animates an interactive matrix with random flipping bits to your website 💥⚡️.

## Demo
![Matrix Bit Animation Demo Gif](./demo_img/demo.gif)

You can also see an interactive live demonstration on my [Website](http://nils-trautner.de).

## Description
This program places random bits which then change between 0 and 1, creating an interactive matrix feel. Generate and place new bits with the method 'addNewBit()' and call the method 'bitsTick()' by a timer in your main class, i.e. with a period of 50 ms. 

## How to use
1. Clone/Download the .ts and .css file and add them to your project
2. Create an object BitAnimation and define, when the bits ought to be displayed, i.e. a mouseover event
3. You need a timer which regulary calls the tick method
4. Fine adjust the animation to your personal preferences and enjoy!
