/**
 * Random Noise Bit Matrix Placer
 * 
 * This program places random bits which then change between 0 and 1, 
 * creating an interactive matrix feel. Generate and place new bits
 * with the method 'addNewBit()' and call the method 'bitsTick()' by
 * a timer in your main class, i.e. with a period of 50 ms. 
 * 
 * @author Nils Trautner
 * @version 1.0
 */

interface Bit {
  pos: { x: number; y: number };
  maxTimeAlive: number;
  timeAlive: number;
  nextStateChangeTime: number;
  digit: number;
  element: HTMLDivElement;
}

interface Position {
  x: number;
  y: number;
}

export class BitAnimation {

  //General declaration
  private lastAddedMouseCords: Position = { x: 0, y: 0 };
  private bits = Array<Bit>();

  //Adjustable
  private readonly MAX_BITS = 150;
  private parentElementId = 'start';
  private bitSize = 30;
  private placeRadius = 2; //0=single place
  private bitsPerPlace = 2;
  private fadeOutOpacity = true;
  private maxTimeAliveRandomSpan = 90;
  private maxTimeAliveMinValue = 20;

  constructor() {}

  /**
   * Generates a collection of new bits around pixelPos
   * @param pixelPos The position on the screen in pixels, where the bits will be displayed
   * @returns 
   */
  public addNewBit(pixelPos: Position): void {
    if (this.bits.length >= this.MAX_BITS) return;

    let gridPos = {
      x: Math.floor((pixelPos.x + window.scrollX) / this.bitSize),
      y: Math.floor((pixelPos.y + window.scrollY) / this.bitSize),
    };
    if (
        gridPos.x === this.lastAddedMouseCords.x &&
        gridPos.y === this.lastAddedMouseCords.y
    )
      return;

    for (let i = 1; i < this.bitsPerPlace; i++) {
      const x = Math.round(Math.random()*this.placeRadius*2 - this.placeRadius);
      const y = Math.round(Math.random()*this.placeRadius*2 - this.placeRadius);

      //Check for other digits at the desired position
      let positionIsFree = true;
      for (let i = 0; i < this.bits.length; i++) {
        if ( this.bits[i].pos.x === gridPos.x + x && this.bits[i].pos.y === gridPos.y + y) {
          positionIsFree = false;
          break;
        }
      }
      if (!positionIsFree) continue;
      
     //Generate new HTML element
     const startDigit = Math.round(Math.random());
     const div = this.addNewElement({x: gridPos.x + x, y: gridPos.y + y}, startDigit);

     //Add new element to list
     this.bits.push({
       pos: { x: gridPos.x + x, y: gridPos.y + y },
       maxTimeAlive: Math.random() * this.maxTimeAliveRandomSpan + this.maxTimeAliveMinValue,
       timeAlive: 0,
       nextStateChangeTime: Math.floor(Math.random() * 10 + 1),
       digit: startDigit,
       element: div,
     });
    }

    //Avoid last mouse position
    this.lastAddedMouseCords = gridPos;
  }

  /**
   * Generates a new HTML Div with an textnode inside and appends it
   * @param coordinates Coordinates of the pixel based on the grid coordinates
   * @returns 
   */
  private addNewElement(coordinates: Position, digit: number): HTMLDivElement {
    let div = document.createElement('div');
    div.classList.add('bit');
    div.classList.add('noselect');

    div.style.left = (coordinates.x) * this.bitSize + 'px';
    div.style.top = (coordinates.y) * this.bitSize + 'px';

    div.appendChild(document.createTextNode(digit.toString()));
    document.getElementById(this.parentElementId)?.appendChild(div);

    return div;
  }

  /**
   * Gets called regulary and handles bit change, alive status and opactiy
   */
  public bitsTick(): void {
    for (let i = 0; i < this.bits.length; i++) {
      //Change bit
      if (this.bits[i].timeAlive > this.bits[i].nextStateChangeTime) {
        this.bits[i].digit = (this.bits[i].digit + 1) % 2;
        this.bits[i].nextStateChangeTime = this.bits[i].timeAlive + Math.floor(Math.random() * 10) + 1;
        this.bits[i].element.innerHTML = this.bits[i].digit.toString();
      }

      //Check if dead
      if (this.bits[i].timeAlive > this.bits[i].maxTimeAlive) {
        this.bits[i].element.remove();
        this.bits.splice(i, 1);
        continue;
      }

      //Adjust opacity
      if (this.fadeOutOpacity) {
        this.bits[i].element.style.opacity = (1 - this.bits[i].timeAlive / this.bits[i].maxTimeAlive).toString();
      }

      this.bits[i].timeAlive++;
    }
  }


}